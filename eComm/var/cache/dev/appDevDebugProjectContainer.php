<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerWvfg5w7\appDevDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerWvfg5w7/appDevDebugProjectContainer.php') {
    touch(__DIR__.'/ContainerWvfg5w7.legacy');

    return;
}

if (!\class_exists(appDevDebugProjectContainer::class, false)) {
    \class_alias(\ContainerWvfg5w7\appDevDebugProjectContainer::class, appDevDebugProjectContainer::class, false);
}

return new \ContainerWvfg5w7\appDevDebugProjectContainer(array(
    'container.build_hash' => 'Wvfg5w7',
    'container.build_id' => '99ab6dd4',
    'container.build_time' => 1542580279,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerWvfg5w7');
